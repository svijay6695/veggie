import { RFValue } from 'react-native-responsive-fontsize';

export const colors = {
  white: '#fff',
  black: '#000',
  green:'#86BC42'
};

export const fontFamliy = {
  bold: 'Raleway-Bold',
  medium: 'Raleway-Medium',
  regular: 'Raleway-Regular'
};

export const fontSize = {
  px4: RFValue(2, 780),
  px8: RFValue(8, 580),
  px10: RFValue(10, 580),
  px12: RFValue(12, 580),
  px14: RFValue(14, 580),
  px16: RFValue(16, 580),
  px18: RFValue(18, 580),
  px20: RFValue(20, 580),
  px22: RFValue(22, 580),
  px24: RFValue(24, 580)
};
