// App level common styles.

import { StyleSheet, Dimensions } from 'react-native';

import { colors, fontFamliy, fontSize } from './properties';
let ScreenHeight = Dimensions.get('window').height;

export default StyleSheet.create({
  body: {
    backgroundColor: colors.green,
    height: ScreenHeight,
  },
  bodyScroll:{
    backgroundColor: colors.green,
    height: ScreenHeight,
  },
  input:{
    borderBottomWidth: 1,
    borderColor: colors.white,
    paddingVertical: 10,
    fontSize: fontSize.px18,
    fontFamily: fontFamliy.medium,
    color: colors.white,
    marginBottom:20,
  },
  whiteButton:{
    fontFamily: fontFamliy.bold,
    fontSize: fontSize.px18,
    lineHeight: 16,
    color: colors.green,
    backgroundColor: colors.white,
    padding: 15,
    borderRadius: 8,
    textAlign: 'center'
  },
  error:{
    color:colors.white,
    fontSize: fontSize.px16,
    fontFamily: fontFamliy.bold,
  }
});
