import React, {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Actions} from 'react-native-router-flux';


import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import styles from '../../styles/common';
import onboardStyle from '../onboarding/onboardstyle'

import dashboardStyle from './dasboardstyle';
import axios from 'axios';

const Viewcard = () => {
  const [cart, setCart] = useState([]);
  const [counter, setCounter] = useState(1);
  const [price, setPrice] = useState();
  const [id, setId] = useState();


  const [tab,setTab] = useState('one');


  const tabSection = tab === 'one' ? onboardStyle.show : onboardStyle.hide;
  const tabSection1 = tab === 'two' ? onboardStyle.show : onboardStyle.hide;

    const ViewCard = () => {

       axios({
      method: 'get',
      url: `http://veggiemami.in/api/cart/list/${id}`,
    }).then(
      (response) => {
        const {data} = response;
        setCart(data);
        console.log(data)
        if (response.data.length === 0){
          setTab('one')
        }
        else{
          setTab('two')
        }
      },
      (error) => {
        console.log(error);
      },
    );

    };
 

  useEffect(() => {
    getData();
    ViewCard();
    // axios({
    //   method: 'get',
    //   url: `http://veggiemami.in/api/cart/list/${id}`,
    // }).then(
    //   (response) => {
    //     const {data} = response;
    //     setCart(data);
    //     if (response.data.length === 0){
    //       setTab('one')
    //     }
    //     else{
    //       setTab('two')
    //     }
    //   },
    //   (error) => {
    //     console.log(error);
    //   },
    // );
  }, []);

  // Get Data From Local Storage
  const getData = async () => {
      try {
        const value = await AsyncStorage.getItem('id');
        setId(value);
        if (value !== null) {
        }
      } catch (e) {}
    };

  const DeleteCard = (item) => {
    const val = item.id;
    axios({
      method: 'get',
      url: `http://veggiemami.in/api/cart/deleteCartItem/${val}`,
    }).then(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.log(error);
      },
    );
  };

  const ClearCard = () => {
    axios({
      method: 'get',
      url: 'http://veggiemami.in/api/cart/deleteCartAllItemsByUser/8',
    }).then(
      (response) => {
        console.log(response);
      
      },
      (error) => {
        console.log(error);
      },
    );
    setTab('one')
  };

  // const CountIncrease = () => {
  //   var itemprice = 10;
  //   setCounter(counter + 1);
  //   setPrice(itemprice * counter)

  // };

  // const CountDecrease = () => {
  //   var itemprice = 10;
  //   setCounter(counter - 1);
  //   setPrice(itemprice * counter)
  //   if (counter === 1){
  //     alert("Item Can't 0")
  //   }
  // };

 



  return (
    <>
      <SafeAreaView>
        <View style={styles.bodyScroll}>
          <View style={dashboardStyle.dashboard}>
            <Text style={dashboardStyle.text}>View Cart</Text>
           
            <View style={tabSection1}>
            <TouchableOpacity onPress={(e) => ClearCard()}>
              <Text style={dashboardStyle.viewCardText}>Clear Cart</Text>
            </TouchableOpacity>
            <View style={dashboardStyle.viewCartHeight}>
            <ScrollView contentInsetAdjustmentBehavior="automatic">
            {cart.map((item) => (
              <View style={dashboardStyle.viewcart} key={item.id}>
                <Text style={dashboardStyle.addButtonText}>{item.item}</Text>
                <View style={dashboardStyle.addBox}>
                  {/* <TouchableOpacity onPress={(e) => CountDecrease()}>
                    <Text style={dashboardStyle.addIconText}>-</Text>
                  </TouchableOpacity> */}

                  <Text style={dashboardStyle.cartText}>{item.quantity}</Text>
                  <Text style={dashboardStyle.cartText}>
                    x
                  </Text>
                  <Text style={dashboardStyle.cartText}>{item.price}</Text>
                  {/* <TouchableOpacity onPress={(e) => CountIncrease()}>
                    <Text style={dashboardStyle.addIconText}>+</Text>
                  </TouchableOpacity> */}
                </View>
                <Text style={dashboardStyle.addButtonText}>
                {(item.price) * (item.quantity)}

                  {/* <TouchableOpacity onPress={(e) => DeleteCard(item)}>
                    <Text style={dashboardStyle.addButtonText}>Delete</Text>
                  </TouchableOpacity> */}
                </Text>
              </View>
            ))}
            </ScrollView>
            </View>
            {/* <TouchableOpacity onPress={e => ViewCard()}>
              <Text style={dashboardStyle.viewCardText}>View Cart</Text>
              </TouchableOpacity> */}
            {/* <TouchableOpacity onPress={e => DeleteCard()}>
              <Text style={dashboardStyle.viewCardText}>Delete Cart</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={e => ClearCard()}>
              <Text style={dashboardStyle.viewCardText}>Clear Cart</Text>
              </TouchableOpacity> */}
              <View style={dashboardStyle.placebutton}>

             <TouchableOpacity onPress={() => {
                  Actions.address();
                }}>
                <Text style={styles.whiteButton}>Place Order</Text>
            </TouchableOpacity> 

              </View>
              </View>
              <View style={tabSection} >
                <Text onPress={e => ViewCard()} style={dashboardStyle.viewCardText}>
                  Load Cart
                </Text>
              </View>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Viewcard;
