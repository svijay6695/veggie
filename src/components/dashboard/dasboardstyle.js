import {StyleSheet} from 'react-native';

import {colors, fontFamliy, fontSize} from '../../styles/properties';

export default StyleSheet.create({
  dashboard:{
    padding:'5%',
    height:'97%'
  },
  text:{
      textAlign:'center',
      color:colors.white,
      fontSize:fontSize.px20,
      fontFamily: fontFamliy.bold,

  },
  viewCartHeight:{
    height:'75%'
  },  
  viewcart:{
    flexDirection:'row',
    justifyContent:'space-between',
    marginTop:20,
    alignItems:'center',
  },
  viewCard:{
    flexDirection:'row',
    justifyContent:'flex-end'
  },
  viewCardText:{
    color:colors.green,
    fontSize:fontSize.px18,
    fontFamily: fontFamliy.bold,
    backgroundColor:colors.white,
    borderRadius:20,
    alignItems:'center',
    textAlign:'center',
    marginTop:20,
    padding:10,
    width:200,
  },
  card:{
      marginTop:20,
      backgroundColor:colors.white,
      padding:10,
      width:'100%'
  },
   
  imgSec:{
    width:'100%',
    height:100
  },    
  img:{
      width:'100%',
      height:'100%'
  },
  imgText:{
      color:colors.black,
      textAlign:'center',
      fontSize:fontSize.px18,
      fontFamily: fontFamliy.bold,
  },
  customCard:{
    backgroundColor:colors.white,
    padding:10,
    borderRadius:10,
    width:'48%',
    marginBottom:20,
  },
  customCardImg:{
    width:'100%',
    height:100
  },
  customCardText:{
    color:colors.black,
    fontSize:fontSize.px14,
    fontFamily: fontFamliy.bold,
  },
  Cards:{
    marginTop:20,
    flexDirection:'row',
    justifyContent:'space-between',
    flexWrap:'wrap',
  },   
  loadText:{
    color:colors.white,
    fontSize:fontSize.px18,
    fontFamily: fontFamliy.medium,
  },
  loadTextSec:{
    justifyContent:'center',
    alignItems:'center',
    flex:1,
  },
  arrowimg:{
    width:30,
    height:30
  },
  arrowSec:{
    marginTop:20,
    flexDirection:'row',
    justifyContent:'space-between'
  },
  topleft:{
    flexDirection:'row',
    alignItems:'center'
  },
  topleftText:{
    marginLeft:'30%'
  },
  addButton:{
    backgroundColor:colors.green,
    flexDirection:'row',
    justifyContent:'center',
    marginTop:10,
    padding:10,
    borderRadius:10,
  },
  addButton1:{
    backgroundColor:colors.green,
    flexDirection:'row',
    justifyContent:'center',
    marginTop:10,
    padding:5,
    borderRadius:10,
  },
  addButtonContent:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',

  },
  addButtonText:{
    color:colors.white,
    fontFamily: fontFamliy.bold,
    fontSize:fontSize.px18,
  },
  addButtonTextCart:{
    color:colors.white,
    fontFamily: fontFamliy.bold,
    fontSize:fontSize.px24,
  },
  addButtonTextSec:{
    paddingHorizontal:20,
    color:colors.white,
    fontFamily: fontFamliy.bold,
    fontSize:fontSize.px14,
  },
  addIconText:{
    color:colors.white,
    fontFamily: fontFamliy.bold,
    fontSize:fontSize.px20,
  },
  addBox:{
    // borderWidth:1,
    // borderColor:colors.white,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    padding:10,
    paddingVertical:0,
  },
 
  cartText:{
    fontSize:fontSize.px18,
    color:colors.white,
    paddingHorizontal:20,
  },
  placebutton:{
    position:'absolute',
    bottom:'-10%',
    width:'100%',
  },
  emptyState:{
    fontSize:fontSize.px18,
    color:colors.white,
  },
  addressCard:{
    backgroundColor:colors.white,
    padding:10,
    borderRadius:10,
    width:'48%',
    marginBottom:20,
  },
  addressCardText:{
    color:colors.black,
    fontSize:fontSize.px18,
    fontFamily: fontFamliy.bold,
  },
  addressCardSec:{
    flexDirection:'row',
    justifyContent:'space-between',
    flexWrap:'wrap',
  },
  addressContainer:{
    height:'95%',
  },
});
