import React, {useState, useEffect} from 'react';

import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import styles from '../../styles/common';
import onboardStyle from '../onboarding/onboardstyle'
import dashboardStyle from './dasboardstyle';
import {Actions} from 'react-native-router-flux';
import Card from './card';
import axios from 'axios';
import {colors} from '../../styles/properties';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { set } from 'react-native-reanimated';

const Fruits = () => {
  const [fruits, setFruits] = useState([]);
  const [loader, setLoader] = useState(false);
  const [id, setId] = useState();
  const [tab,setTab] = useState('one');
  const [cardlist, setCardlist] = useState([]);

  const tabSection = tab === 'one' ? onboardStyle.show : onboardStyle.hide;
  const tabSection1 = tab === 'two' ? onboardStyle.show : onboardStyle.hide;

  const [counter,setCounter] = useState(1)

  useEffect(() => {
    setLoader(true);
    getData();
    axios({
      method: 'get',
      url: `http://veggiemami.in/api/itemsbycategory/fruits`,
    }).then(
      (response) => {
        setLoader(false);
        const {data} = response.data;
        setFruits(data);
      },
      (error) => {
        // console.log(error);
        setLoader(false);
      },
    );
  }, []);

  const clickHome = () => {
    Actions.dashboard();
  };

  // Get Data From Local Storage
  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('id');
      setId(value);
      if (value !== null) {
      }
    } catch (e) {}
  };

  const AddCard = (item) => {
    setCardlist(item);
    setTab('two')
    axios({
      method: 'post',
      url: 'http://veggiemami.in/api/cart/addItemCart/',
      data: {
        user_id: id,
        item_id: item.id,
        quantity: counter,
      },
    }).then(
      (response) => {
        console.log(response)
      },
      (error) => {
        // console.log(error);
      },
    );
  };

  const ViewCard = () => {
    Actions.view();
  };

  const CountIncrease = () => { 
    setCounter(counter+1)
  };


  const CountDecrease = () => {
    setCounter(counter-1);
    if (counter === 1){
      setTab('one')
      setCounter(1)
    }
  };

 

  console.log(counter)


  return (
    <>
      <SafeAreaView>
        <View style={styles.bodyScroll}>
          <View style={dashboardStyle.dashboard}>
            <View style={dashboardStyle.topleft}>
              <TouchableOpacity onPress={clickHome}>
                <View>
                  <Image source={require('../../assets/images/topleft.png')} />
                </View>
              </TouchableOpacity>
              <View style={dashboardStyle.topleftText}>
                <Text style={dashboardStyle.text}>Fruits</Text>
              </View>
            </View>
            <ScrollView contentInsetAdjustmentBehavior="automatic">
              <View style={dashboardStyle.Cards}>
                {loader ? (
                  <View style={dashboardStyle.loadTextSec}>
                    <Text style={dashboardStyle.loadText}> Loading...</Text>
                  </View>
                ) : (
                  fruits.map((item) => (
                    <View style={dashboardStyle.customCard} key={item.key}>
                      <View style={dashboardStyle.customCardImg}>
                        <Image
                          style={dashboardStyle.img}
                          source={{
                            uri: `http://veggiemami.in/images/${item.image}`,
                          }}
                        />
                      </View>
                      <View>
                        <Text style={dashboardStyle.customCardText}>
                          {item.name}
                        </Text>
                      </View>
                      <View>
                        <Text style={dashboardStyle.customCardText}>
                          {item.tamilname}
                        </Text>
                      </View>
                      <View>
                        <Text style={dashboardStyle.customCardText}>
                          {item.price}
                        </Text>
                      </View>
                      <View style={tabSection}>
                      <View style={dashboardStyle.addButton}>
                        <TouchableOpacity onPress={(e) => AddCard(item)}>
                          <View style={dashboardStyle.addButtonContent}>
                            
                            <Text style={dashboardStyle.addButtonTextSec}>
                             Add
                            </Text>
                            
                          </View>
                        </TouchableOpacity>
                      </View>
                      </View>
                      <View style={tabSection1}>
                      <View style={dashboardStyle.addButton1}>
                        <TouchableOpacity onPress={(e) => AddCard(item)}>
                          <View style={dashboardStyle.addButtonContent}>
                            <TouchableOpacity onPress={(e) => CountDecrease()}>
                              <Text style={dashboardStyle.addButtonTextCart}>
                                -
                              </Text>
                            </TouchableOpacity>
                            <Text style={dashboardStyle.addButtonTextSec}>
                              {counter}
                            </Text>
                            <TouchableOpacity onPress={(e) => CountIncrease()}>
                              <Text style={dashboardStyle.addButtonTextCart}>
                                +
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </TouchableOpacity>
                      </View>
                      </View>
                    </View>
                  ))
                )}
              </View>
            </ScrollView>
            <View style={dashboardStyle.viewCard}>
              {/* <Text style={dashboardStyle.viewCardText}>
                {cardlist.id}
              </Text> */}
              {/* <Text style={dashboardStyle.viewCardText}>
                  {cardlist.name}   
              </Text> */}
              <TouchableOpacity onPress={(e) => ViewCard()}>
                <Text style={dashboardStyle.viewCardText}>View Cart</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Fruits;
