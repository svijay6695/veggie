import React, {useState, useEffect} from 'react';

import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import styles from '../../styles/common';
import dashboardStyle from './dasboardstyle';
import {Actions} from 'react-native-router-flux';
import Card from './card';
import axios from 'axios';

const Organic = () => {
  const [organic, setOrganic] = useState([]);
  const [loader, setLoader] = useState(false);
  const [page, setPage] = useState(1);
  const [cardlist,setCardlist] = useState([]);

  

  useEffect(() => {
    setLoader(true);
    axios({
      method: 'get',
      url: `http://veggiemami.in/api/itemsbycategory/organic?page=${page}`,
    }).then(
      (response) => {
        setLoader(false);
        const {data} = response.data;
        setOrganic(data);
      },
      (error) => {
        console.log(error);
        setLoader(false);
      },
    );
  }, [page]);

  const leftArrow = () => {
    setPage(1);
  };

  const clickHome = () => {
    Actions.dashboard();
  };

  const rightArrow = () => {
    setPage(2);
  };

  const AddCard = (item) => {
    setCardlist(item)
  };


  return (
    <>
      <SafeAreaView>
        <View style={styles.bodyScroll}>
          <View style={dashboardStyle.dashboard}>
            <View style={dashboardStyle.topleft}>
            <TouchableOpacity onPress={clickHome}>

              <View>
                <Image source={require('../../assets/images/topleft.png')} />
              </View>
              </TouchableOpacity>
              <View  style={dashboardStyle.topleftText}>
                <Text style={dashboardStyle.text}>Organics</Text>
              </View>
            </View>
            <ScrollView contentInsetAdjustmentBehavior="automatic">
              <View style={dashboardStyle.Cards}>
                {loader ? (
                  <View style={dashboardStyle.loadTextSec}>
                    <Text style={dashboardStyle.loadText}> Loading...</Text>
                  </View>
                ) : (
                  organic.map((item) => (
                    <View style={dashboardStyle.customCard} key={item.key}>
                      <View style={dashboardStyle.customCardImg}>
                        <Image
                          style={dashboardStyle.img}
                          source={{
                            uri: `http://veggiemami.in/images/${item.image}`,
                          }}
                        />
                      </View>
                      <View>
                        <Text style={dashboardStyle.customCardText}>
                          {item.name}
                        </Text>
                      </View>
                      <View>
                        <Text style={dashboardStyle.customCardText}>
                          {item.tamilname}
                        </Text>
                      </View>
                      <View>
                        <Text style={dashboardStyle.customCardText}>
                          {item.price}
                        </Text>
                      </View>
                      <View style={dashboardStyle.addButton}>
                    <TouchableOpacity onPress={e => AddCard(item)}>
                      <Text style={dashboardStyle.addButtonText}>
                          Add  
                      </Text>
                   </TouchableOpacity>
                    </View>
                    </View>
                  ))
                )}
              </View>
            </ScrollView>
            <View style={dashboardStyle.viewCard}>
              <Text style={dashboardStyle.viewCardText}>
                {cardlist.id}
              </Text>
              <Text style={dashboardStyle.viewCardText}>
                  {cardlist.name}   
              </Text>
              <Text style={dashboardStyle.viewCardText}>View Cart</Text>
            </View>
            <View style={dashboardStyle.arrowSec}>
              <TouchableOpacity onPress={leftArrow}>
                <View>
                  <Image
                    style={dashboardStyle.arrowimg}
                    source={require('../../assets/images/left.png')}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={rightArrow}>
                <View>
                  <Image
                    style={dashboardStyle.arrowimg}
                    source={require('../../assets/images/right.png')}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Organic;
