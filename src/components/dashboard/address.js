import React, {useState, useEffect} from 'react';
import axios from 'axios';

import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import styles from '../../styles/common';
import {colors} from '../../styles/properties';
import onboardStyle from '../onboarding/onboardstyle';
import dashboardStyle from '../dashboard/dasboardstyle';

import {Actions} from 'react-native-router-flux';
import {useForm, Controller} from 'react-hook-form';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Address = () => {
  const {control, handleSubmit, errors} = useForm();

  const [mobileerror, setMobileerror] = useState(true);
  const [passworderror, setPassworderror] = useState(true);
  const [id, setId] = useState();
  const [address, setAddress] = useState([]);

  const error1 = mobileerror === false ? onboardStyle.show : onboardStyle.hide;
  const error2 =
    passworderror === false ? onboardStyle.show : onboardStyle.hide;

  useEffect(() => {
    getData();
    onAddress();
  });

  const onAddress = () => {
    axios({
      method: 'get',
      url: `http://veggiemami.in/api/list/address/${id}`,
    }).then(
      (response) => {
        const data = response.data;
        setAddress(data);
      },
      (error) => {
        console.log(error);
      },
    ),
      [];
  };

  const onSubmit = (data) => {
    axios({
      method: 'post',
      url: `http://veggiemami.in/api/saveAddress/`,
      data: {
        userid: id,
        addressLine1: data.address,
        addressLine2: data.address1,
        pincode: data.pincode,
      },
    }).then(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.log(error);
      },
    );
  };

  // Get Data From Local Storage
  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('id');
      setId(value);
      if (value !== null) {
      }
    } catch (e) {}
  };

  const addressRef = React.useRef();
  const address1Ref = React.useRef();
  const pincodeRef = React.useRef();

  return (
    <>
      <SafeAreaView>

        <View style={styles.body}>
          <View style={onboardStyle.formContainer}>
          <View style={dashboardStyle.addressContainer}>

                {/* <View style={onboardStyle.enteraddress}>
                  <Text style={onboardStyle.address}>Select Address</Text>
                </View> */}
                {/* <View style={dashboardStyle.addressCardSec}>
                  {address.map((item) => (
                    <View style={dashboardStyle.addressCard} key={item.key}>
                      <View>
                        <Text style={dashboardStyle.addressCardText}>
                          {item.addressLine1}
                        </Text>
                      </View>
                      <View>
                        <Text style={dashboardStyle.addressCardText}>
                          {item.addressLine2}
                        </Text>
                      </View>
                      <View>
                        <Text style={dashboardStyle.addressCardText}>
                          {item.pincode}
                        </Text>
                      </View>
                    </View>
                  ))}
                </View> */}
                <View style={onboardStyle.enteraddress}>
                  <Text style={onboardStyle.address}>Enter Address Details</Text>
                </View>
                <Controller
                  control={control}
                  onFocus={() => {
                    adddressRef.current.focus();
                  }}
                  render={({onChange, onBlur, value}) => (
                    <TextInput
                      style={styles.input}
                      onChangeText={(value) => onChange(value)}
                      value={value}
                      placeholder="Address Line 1"
                      placeholderTextColor={colors.white}
                      ref={addressRef}
                    />
                  )}
                  name="address"
                  rules={{required: true}}
                  defaultValue=""
                />
                {errors.address && (
                  <Text style={styles.error}>Address is required.</Text>
                )}

                <Controller
                  control={control}
                  onFocus={() => {
                    address1Ref.current.focus();
                  }}
                  render={({onChange, onBlur, value}) => (
                    <TextInput
                      style={styles.input}
                      onChangeText={(value) => onChange(value)}
                      value={value}
                      placeholder="Address Line 2"
                      placeholderTextColor={colors.white}
                      ref={address1Ref}
                    />
                  )}
                  name="address1"
                  rules={{required: true}}
                  defaultValue=""
                />

                <Controller
                  control={control}
                  onFocus={() => {
                    pincodeRef.current.focus();
                  }}
                  render={({onChange, onBlur, value}) => (
                    <TextInput
                      style={styles.input}
                      keyboardType="numeric"
                      onChangeText={(value) => onChange(value)}
                      value={value}
                      placeholder="Pincode"
                      placeholderTextColor={colors.white}
                      ref={pincodeRef}
                      maxLength={6}
                    />
                  )}
                  name="pincode"
                  rules={{required: true}}
                  defaultValue=""
                />
                {errors.pincode && (
                  <Text style={styles.error}>Pincode is required.</Text>
                )}
                <View style={onboardStyle.submitBut}>
                  <TouchableOpacity onPress={handleSubmit(onSubmit)}>
                    <Text style={styles.whiteButton}>Submit</Text>
                  </TouchableOpacity>
                </View>
            </View>

          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Address;
