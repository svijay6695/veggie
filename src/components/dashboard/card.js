import React from 'react';

import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import styles from '../../styles/common';
import dashboardStyle from './dasboardstyle';
import {Actions} from 'react-native-router-flux';

const Card = (img, price, name, tamilname) => {
  return (
    <>
      <View style={dashboardStyle.customCard}>
        {/* <View style={dashboardStyle.customCardImg}>
          <Image style={dashboardStyle.img} source={item.image} />
        </View> */}
        <View>
          <Text style={dashboardStyle.customCardText}>{name}</Text>
        </View>
        <View>
      <Text style={dashboardStyle.customCardText}>{price}</Text>
        </View>
        <View>
      <Text style={dashboardStyle.customCardText}>{tamilname}</Text>
        </View>
      </View>
    </>
  );
};

export default Card;
