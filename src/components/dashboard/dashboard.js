import React from 'react';

import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import styles from '../../styles/common';
import dashboardStyle from './dasboardstyle';
import {Actions} from 'react-native-router-flux';

const Dashboard = () => {
  return (
    <>
      <SafeAreaView>
        <View style={styles.body}>
          <View style={dashboardStyle.dashboard}>
            <Text style={dashboardStyle.text}>Category</Text>
            <ScrollView contentInsetAdjustmentBehavior="automatic">
            <View style={dashboardStyle.CardSec}>
              <View style={dashboardStyle.card}>
                <TouchableOpacity
                  onPress={() => {
                    Actions.fruits();
                  }}>
                  <View style={dashboardStyle.imgSec}>
                    <Image
                      style={dashboardStyle.img}
                      source={require('../../assets/images/fruits.jpeg')}
                    />
                  </View>
                  <View>
                    <Text style={dashboardStyle.imgText}>View More</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={dashboardStyle.card}>
                <TouchableOpacity
                  onPress={() => {
                    Actions.vegetables();
                  }}>
                  <View style={dashboardStyle.imgSec}>
                    <Image
                      style={dashboardStyle.img}
                      source={require('../../assets/images/vegetables.jpeg')}
                    />
                  </View>
                  <View>
                    <Text style={dashboardStyle.imgText}>View More</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={dashboardStyle.CardSec}>
              <View style={dashboardStyle.card}>
                <TouchableOpacity
                  onPress={() => {
                    Actions.greens();
                  }}>
                  <View style={dashboardStyle.imgSec}>
                    <Image
                      style={dashboardStyle.img}
                      source={require('../../assets/images/greens.jpeg')}
                    />
                  </View>
                  <View>
                    <Text style={dashboardStyle.imgText}>View More</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={dashboardStyle.card}>
                <TouchableOpacity
                  onPress={() => {
                    Actions.organic();
                  }}>
                  <View style={dashboardStyle.imgSec}>
                    <Image
                      style={dashboardStyle.img}
                      source={require('../../assets/images/organic.jpeg')}
                    />
                  </View>
                  <View>
                    <Text style={dashboardStyle.imgText}>View More</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Dashboard;
