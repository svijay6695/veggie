import {StyleSheet} from 'react-native';

import {colors, fontFamliy, fontSize} from '../../styles/properties';

export default StyleSheet.create({
  show: {
    display: 'flex'
  },
  hide: {
    display: 'none'
  },
  logoContainer: {
    alignItems: 'center'
  },
  formContainer:{
    paddingHorizontal: 30,
    marginTop:20,
  },
  buttonContainer:{
      marginTop:20,
  },
  registerContainer:{
    flexDirection: 'row',
    justifyContent:'center',
    marginTop:30,
  },
  account:{
    fontFamily: fontFamliy.regular,
    fontSize: fontSize.px14,
    lineHeight: 20,
    color: colors.black,
    marginRight:20
  },
  register:{
    fontFamily: fontFamliy.bold,
    fontSize: fontSize.px14,
    lineHeight: 20,
    color: colors.white,
  },
  resendOtp:{
    flex:1,
    justifyContent:'flex-end',
    alignItems:'flex-end',
    marginTop:5,
  },
  dashboard:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  address:{
    color:colors.white,
    fontFamily: fontFamliy.bold,
    fontSize:fontSize.px20,
  },
  enteraddress:{
    marginBottom:20,
  },
  submitBut:{
    marginTop:40
  }
});
