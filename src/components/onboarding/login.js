import React, {useState} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';



import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import styles from '../../styles/common';
import {colors} from '../../styles/properties';
import onboardStyle from './onboardstyle';
import {Actions} from 'react-native-router-flux';
import {useForm, Controller} from 'react-hook-form';

const Login = () => {
  const {control, handleSubmit, errors} = useForm();
  const [status, setStatus] = useState([]);
  const [text, setText] = useState(true);

  const error = text === false ? onboardStyle.show : onboardStyle.hide;


  // Local Storage
  // const storeData = async () => {
  //   try {
  //     await AsyncStorage.setItem('id', "8")
  //   } catch (e) {
  //     // saving error
  //   }
  // }

  // const getData = async () => {

  //   try {
  //     const value = await AsyncStorage.getItem('id')
  //      console.log(value)
  //     if(value !== null) {
  //       // value previously stored
  //     }
  //   } catch(e) {
  //     // error reading value
  //   }
  // }

  

  const onSubmit = async (data) => {
    await axios({
      method: 'post',
      url: 'http://veggiemami.in/api/loginuser',
      data: {
        mobileno: data.mobileno,
        password: data.password,
      },
    }).then(
      (response) => {
        AsyncStorage.setItem('id', response.data.id)
        const {status} = response.data;    
        if (status === 'Valid') {
          Actions.dashboard();
          setText(true)
        } 
        else {
          setText(false)
        }
      },
      (error) => {
        console.log(error);
      },
    );
  };


  const mobileRef = React.useRef();
  const passwordRef = React.useRef();

  return (
    <>
      <SafeAreaView>
        <View style={styles.body}>
          <View style={onboardStyle.logoContainer}>
            <Image source={require('../../assets/images/logo.png')} />
          </View>
          <View style={onboardStyle.formContainer}>
            <Controller
              control={control}
              onFocus={() => {
                mobileRef.current.focus();
              }}
              render={({onChange, onBlur, value}) => (
                <TextInput
                  style={styles.input}
                  keyboardType="numeric"
                  onChangeText={(value) => onChange(value)}
                  value={value}
                  placeholder="Mobile Number"
                  placeholderTextColor={colors.white}
                  ref={mobileRef}
                  maxLength={10}
                />
              )}
              name="mobileno"
              rules={{required: true}}
              defaultValue=""
            />
            {errors.mobileno && (
              <Text style={styles.error}>Mobile Number is required.</Text>
            )}

            <Controller
              control={control}
              onFocus={() => {
                passwordRef.current.focus();
              }}
              render={({onChange, onBlur, value}) => (
                <TextInput
                  style={styles.input}
                  secureTextEntry={true}
                  onChangeText={(value) => onChange(value)}
                  value={value}
                  placeholder="Password"
                  maxLength={12}
                  placeholderTextColor={colors.white}
                  ref={passwordRef}
                />
              )}
              name="password"
              rules={{required: true}}
              defaultValue=""
            />
            {errors.password && (
              <Text style={styles.error}>Password is required.</Text>
            )}

            <View style={error}>
              <Text style={styles.error}>Enter Valid Login Details</Text>
            </View>

            <View style={onboardStyle.buttonContainer}>
              <TouchableOpacity onPress={handleSubmit(onSubmit)}>
                <Text style={styles.whiteButton}>Login</Text>
              </TouchableOpacity>
            </View>

            <View style={onboardStyle.registerContainer}>
              <Text style={onboardStyle.account}>Don't have Account</Text>
              <TouchableOpacity
                onPress={() => {
                  Actions.register();
                }}>
                <Text style={onboardStyle.register}>Register</Text>
              </TouchableOpacity>
            </View>
           
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Login;
