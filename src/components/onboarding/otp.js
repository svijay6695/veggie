import React from 'react';

import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import styles from '../../styles/common';
import {colors} from '../../styles/properties';
import onboardStyle from './onboardstyle';
import {Actions} from 'react-native-router-flux';

const Otp = () => {
  return (
    <>
      <SafeAreaView>
          <View style={styles.body}>
            <View style={onboardStyle.logoContainer}>
              <Image source={require('../../assets/images/logo.png')} />
            </View>
            <View style={onboardStyle.formContainer}>
              <TextInput
                style={styles.input}
                placeholder="Enter OTP"
                placeholderTextColor={colors.white}></TextInput>
              <View style={onboardStyle.resendOtp}>
                <Text style={onboardStyle.register}>Resend OTP</Text>
              </View>
              <View style={onboardStyle.buttonContainer}>
                <TouchableOpacity
                  onPress={() => {
                    Actions.dashboard();
                  }}>
                  <Text style={styles.whiteButton}>Verify OTP</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
      </SafeAreaView>
    </>
  );
};

export default Otp;
