import React, {useState} from 'react';
import axios from 'axios';

import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import styles from '../../styles/common';
import {colors} from '../../styles/properties';
import onboardStyle from './onboardstyle';
import {Actions} from 'react-native-router-flux';
import {useForm, Controller} from 'react-hook-form';


const Register = () => {
  const {control, handleSubmit, errors} = useForm();
  const [tab, setTab] = useState('one');
  const [text, setText] = useState(true);
  const [mobileerror, setMobileerror] = useState(true);
  const [passworderror, setPassworderror] = useState(true);
  const [randomNumber, setRandomNumber] = useState();

  const error = text === false ? onboardStyle.show : onboardStyle.hide;

  const error1 = mobileerror === false ? onboardStyle.show : onboardStyle.hide;
  const error2 =
    passworderror === false ? onboardStyle.show : onboardStyle.hide;

  const tabSection = tab === 'one' ? onboardStyle.show : onboardStyle.hide;
  const tabSection1 = tab === 'two' ? onboardStyle.show : onboardStyle.hide;

  const onSubmit = (data) => {
    // Register
    axios({
      method: 'post',
      url: 'http://veggiemami.in/api/registeruser',
      data: {
        mobileno: data.mobileno,
        password: data.password,
      },
    }).then(
      (response) => {},
      (error) => {
        console.log(error);
      },
    );

    const mobilenumber = data.mobileno;
    const password = data.password;

    if (mobilenumber.length > 9 && password.length >= 8) {
      setPassworderror(true);
      // OTP Send
      const randomno = Math.floor(100000 + Math.random() * 900000);

      axios({
        method: 'post',
        url:
          'http://txt.stopnotsms.com/api/v2/sms/template?access_token=25d64e2fec114e92ba2d69d4cead2136',
        data: {
          service: 'T',
          template_id: 'c714e381-7323-41cb-b31b-49873b356bf2',
          variables: [randomno],
          to: data.mobileno,
        },
      }).then(
        (response) => {
          setTab('two');
        },
        (error) => {
          console.log(error);
        },
      );
      setRandomNumber(randomno);
    } else {
      setPassworderror(false);

    }

    // if (password.length < 8) {
    //   setPassworderror(false);
    // } else if (password.length >= 8) {
    //   setPassworderror(true);
    // }
  };

  const onOtp = (data) => {
    if (randomNumber == data.otp) {
      Actions.dashboard();
    } else {
      setText(false);
    }
    if (data.otp === '') {
      setText(false);
    }
  };

  const mobileRef = React.useRef();
  const passwordRef = React.useRef();
  const otpRef = React.useRef();

  return (
    <>
      <SafeAreaView>
        <View style={styles.body}>
          <View style={onboardStyle.logoContainer}>
            <Image source={require('../../assets/images/logo.png')} />
          </View>
          <View style={onboardStyle.formContainer}>
            <View style={tabSection}>
              <Controller
                control={control}
                onFocus={() => {
                  mobileRef.current.focus();
                }}
                render={({onChange, onBlur, value}) => (
                  <TextInput
                    style={styles.input}
                    keyboardType="numeric"
                    onChangeText={(value) => onChange(value)}
                    value={value}
                    placeholder="Mobile Number"
                    placeholderTextColor={colors.white}
                    ref={mobileRef}
                    maxLength={10}
                  />
                )}
                name="mobileno"
                rules={{required: true}}
                defaultValue=""
              />
              {errors.mobileno && (
                <Text style={styles.error}>Mobile Number is required.</Text>
              )}
              {/* <View style={error1}>
                <Text style={styles.error}>
                  Mobile Number must be 10 Number
                </Text>
              </View> */}

              <Controller
                control={control}
                onFocus={() => {
                  passwordRef.current.focus();
                }}
                render={({onChange, onBlur, value}) => (
                  <TextInput
                    style={styles.input}
                    secureTextEntry={true}
                    onChangeText={(value) => onChange(value)}
                    value={value}
                    placeholder="Password"
                    maxLength={12}
                    placeholderTextColor={colors.white}
                    ref={passwordRef}
                  />
                )}
                name="password"
                rules={{required: true}}
                defaultValue=""
              />
              {errors.password && (
                <Text style={styles.error}>Password is required.</Text>
              )}
              <View style={error2}>
                <Text style={styles.error}>Mobile Number Must be 10 Digit & Password Must be a 8 Character</Text>
              </View>
            </View>
            <View style={tabSection1}>
              <Controller
                control={control}
                onFocus={() => {
                  otpRef.current.focus();
                }}
                render={({onChange, onBlur, value}) => (
                  <TextInput
                    style={styles.input}
                    keyboardType="numeric"
                    onChangeText={(value) => onChange(value)}
                    value={value}
                    placeholder="Enter OTP"
                    placeholderTextColor={colors.white}
                    ref={otpRef}
                    maxLength={6}
                  />
                )}
                name="otp"
                defaultValue=""
              />
              <View style={error}>
                <Text style={styles.error}>Enter Valid OTP</Text>
              </View>
              {/* <View style={onboardStyle.resendOtp}>
                <Text style={onboardStyle.register}>Resend OTP</Text>
              </View> */}
            </View>

            <View style={onboardStyle.buttonContainer}>
              <View style={tabSection}>
                <TouchableOpacity onPress={handleSubmit(onSubmit)}>
                  <Text style={styles.whiteButton}>Register</Text>
                </TouchableOpacity>
              </View>
              <View style={tabSection1}>
                <TouchableOpacity onPress={handleSubmit(onOtp)}>
                  <Text style={styles.whiteButton}>Verify OTP</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Register;
