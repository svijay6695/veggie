import React from 'react';
import { View } from 'react-native';
import {Scene, Router} from 'react-native-router-flux';
import Address from '../components/dashboard/address';
import Card from '../components/dashboard/card';
import Confirm from '../components/dashboard/confirmorder';
import Dashboard from '../components/dashboard/dashboard';
import Fruits from '../components/dashboard/fruits';
import Greens from '../components/dashboard/greens';
import Organic from '../components/dashboard/organic';
import Vegetables from '../components/dashboard/vegetables';
import Viewcard from '../components/dashboard/viewcard';
import Login from '../components/onboarding/login';
import Otp from '../components/onboarding/otp';
import Register from '../components/onboarding/register';



const Routes = () => (
  
  <Router>
    <Scene key="root">
      <Scene
        key="login"
        component={Login}
        hideNavBar={true}
        title="login"
        panHandlers={null}

      />
       <Scene
        key="login"
        component={Login}
        hideNavBar={true}
        title="login"
        panHandlers={null}
 
      />
       <Scene
        key="register"
        component={Register}
        hideNavBar={true}
        title="register"
        panHandlers={null}


      />
        <Scene
        key="otp"
        component={Otp}
        hideNavBar={true}
        title="otp"
        panHandlers={null}
      />
      <Scene
        key="view"
        component={Viewcard}
        hideNavBar={true}
        title="viewcard"
        panHandlers={null}


      />  
       <Scene
        key="dashboard"
        component={Dashboard}
        hideNavBar={true}
        title="dashboard"
        panHandlers={null}

      />
       <Scene
        key="fruits"
        component={Fruits}
        hideNavBar={true}
        title="fruits"
        panHandlers={null}
        initial


      />

      <Scene
        key="vegetables"
        component={Vegetables}
        hideNavBar={true}
        title="vegetables"
        panHandlers={null}

      />
       <Scene
        key="greens"
        component={Greens}
        hideNavBar={true}
        title="greens"
        panHandlers={null}

      />
       <Scene
        key="organic"
        component={Organic}
        hideNavBar={true}
        title="organic"
        panHandlers={null}


      />
       <Scene
        key="address"
        component={Address}
        hideNavBar={true}
        title="address"
        panHandlers={null}

      />

      <Scene
        key="confirm"
        component={Confirm}
        hideNavBar={true}
        title="confirm"
        panHandlers={null}
      />
    </Scene>
  </Router>
);

export default Routes;
